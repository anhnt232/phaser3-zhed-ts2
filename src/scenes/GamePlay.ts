import 'phaser';
import CONFIG from "../../config";

var gameOptions = {
    tileSize: 100,
    gameWidth: 800,
    gameHeight: 1000
}
var levels = [[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,1,10,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,2,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,10,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,1,1,0,10,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,10,0,0,0],[0,0,0,0,0,0,0,0],[0,0,2,0,0,0,0,0],[0,0,0,0,2,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,2,0,0,10,0],[0,0,0,0,0,0,0,0],[0,2,0,0,0,0,0,0],[0,0,1,0,2,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,10,0,0,0,3,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,2,0,0,0,0,0],[0,0,0,0,0,3,0,0],[0,0,0,2,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,1,0,0,0,0],[0,10,0,0,0,0,1,0],[0,0,0,0,0,1,0,0],[0,0,2,0,2,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,1,1,0,0],[0,1,0,0,0,0,10,0],[0,0,0,1,1,0,0,0],[0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,1,2,0,0],[0,0,0,1,0,0,0,0],[0,0,1,0,0,0,0,0],[0,0,0,0,0,0,3,0],[0,0,10,0,0,0,0,0],[0,0,0,0,0,0,0,0]],[[0,0,0,0,0,0,0,0],[0,0,0,2,0,0,0,0],[0,0,0,0,1,2,0,0],[0,0,0,0,0,0,0,0],[0,0,2,0,0,0,0,0],[0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,10,0,0]]]

var currentLevel = 1;

export default class GamePlay extends Phaser.Scene{
    currentLevel: any;
    undoArray: any[];
    localLevel: any[];
    tileGroup: Phaser.GameObjects.Group;
    stepsGroup: any;
    selectedRow: any;
    selectedCol: any;

    constructor(){
        super("GamePlay");
    }

    preload(){
        this.load.spritesheet("tiles", "assets/tiles.png", {
            frameWidth: gameOptions.tileSize, 
            frameHeight: gameOptions.tileSize});
    }
    create(){
        this.currentLevel = currentLevel;
        
        this.undoArray = [];
        this.drawLevel();
        this.input.on( "pointerdown", this.handleTap, this);
    }
    drawLevel(){
        this.localLevel = [];
        this.tileGroup = this.add.group();
        this.stepsGroup = this.add.group();
        for(var i = 0; i < levels[this.currentLevel].length; i++){
            this.localLevel[i] = [];
            for(var j = 0; j < levels[this.currentLevel][i].length; j++){
                var tile = this.add.sprite(j * gameOptions.tileSize, i * gameOptions.tileSize, "tiles", levels[this.currentLevel][i][j]);
                if(levels[this.currentLevel][i][j] == 0){
                    tile.alpha = 0.6 + Phaser.Math.FloatBetween(0, 0.3);
                }
                this.tileGroup.add(tile);
                this.localLevel[i][j] = {
                    sprite: tile,
                    value: levels[this.currentLevel][i][j]
                }
                var step = this.add.sprite(j * gameOptions.tileSize, i * gameOptions.tileSize, "tiles", 11) as any;
                step.row = i;
                step.col = j;
                step.visible = false;
                this.stepsGroup.add(step);
            }
        }
        // var restartButton = this.add.button(this.scale.width / 2 - gameOptions.tileSize / 2, this.scale.height - gameOptions.tileSize * 1.5, "tiles", function(){
        //     this.scene.start("GamePlay", true, false, this.currentLevel);
        // }, this, 13, 13);

        const playBtn = this.add
            .rectangle(this.scale.width / 2 - gameOptions.tileSize / 2, this.scale.height - gameOptions.tileSize * 1.5, 0xffca27)
            .setInteractive({ useHandCursor: true });

        const playBtnText = this.add
            .text(this.scale.width / 2 - gameOptions.tileSize / 2, this.scale.height - gameOptions.tileSize * 1.5, "Restart", {
                fontFamily: "Arial",
                fontSize: "40px",
            })
            .setOrigin(0.5);

        playBtn.on("pointerdown", () => {
            
            this.scene.start("GamePlay");
        });
    }
    handleTap(pointer, doubleTap){
        var turnTiles = false;
        var tileTapRow = Math.floor(pointer.y / gameOptions.tileSize);
        var tileTapCol = Math.floor(pointer.x / gameOptions.tileSize);
        if(tileTapRow < 0 || tileTapCol < 0 || tileTapRow >= this.localLevel.length || tileTapCol >= this.localLevel[tileTapRow].length){
            return;
        }
        var tileValue = this.localLevel[tileTapRow][tileTapCol].value;
        if(tileValue == 11 || tileValue == 21){
            turnTiles = true;
            this.localLevel[this.selectedRow][this.selectedCol].sprite.frame = 12;
            this.localLevel[this.selectedRow][this.selectedCol].value = 11;
            var deltaX = Phaser.Math.Clamp(tileTapCol - this.selectedCol, -1, 1);
            var deltaY = Phaser.Math.Clamp(tileTapRow - this.selectedRow, -1, 1);
        }
        this.stepsGroup.getChildren().forEach((item) => {
            if(item.visible){
                if(!turnTiles){
                    this.localLevel[item.row][item.col].value -= 11;
                }
                else{
                    if((deltaX > 0 && item.col > this.selectedCol) || (deltaX < 0 && item.col < this.selectedCol) || (deltaY > 0 && item.row > this.selectedRow) || (deltaY < 0 && item.row < this.selectedRow)){
                        this.localLevel[item.row][item.col].sprite.frame = 12;
                        if(this.localLevel[item.row][item.col].value == 21){
                            // this.time.events.add(Phaser.Time.TimerEvent, () =>{
                            //         this.scene.start("GamePlay");
                            // }, this);
                        }
                    }
                    else{
                        this.localLevel[item.row][item.col].value -= 11;
                    }
                }
                item.visible = false;
            }
        }, this);
        if(tileValue > 0 && tileValue < 10){
            this.showSteps(tileTapRow, tileTapCol, tileValue, 0, 1);
            this.showSteps(tileTapRow, tileTapCol, tileValue, 0, -1);
            this.showSteps(tileTapRow, tileTapCol, tileValue, 1, 0);
            this.showSteps(tileTapRow, tileTapCol, tileValue, -1, 0);
            this.selectedRow = tileTapRow;
            this.selectedCol = tileTapCol;
        }
    }
    showSteps(row, col, steps, deltaRow, deltaCol){
        var stepsMade = 0;
        while(stepsMade < steps){
            while(this.localLevel[row][col].value != 0 && this.localLevel[row][col].value != 10){
                row += deltaRow;
                col += deltaCol;
                if(row < 0 || col < 0 || row >= this.localLevel.length || col >= this.localLevel[row].length){
                    return;
                }
            }
            this.stepsGroup.getChildren()[row * this.localLevel[col].length + col].visible = true;
            this.localLevel[row][col].value += 11;
            stepsMade ++;
            row += deltaRow;
            col += deltaCol;
            if(row < 0 || col < 0 || row >= this.localLevel.length || col >= this.localLevel[row].length){
                break;
            }
        }
    }


}

